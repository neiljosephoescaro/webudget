export default function ToString(number) {
	let totalStr = String(number);
	let totalArr = [...totalStr];
	let n, totalArrWithComa = [];
	if(totalArr[0] !== '-') {
		for(n = 1 ; n <= totalArr.length ; n++) {
			if(n % 3 !== 0 || n === totalArr.length) {
				totalArrWithComa.unshift(totalArr[totalArr.length - n]);
			} else {
				totalArrWithComa.unshift(',' + totalArr[totalArr.length - n]);
			}
		}
	} else {
		totalArr.shift(totalArr[0]);
		for(n = 1 ; n <= totalArr.length ; n++) {
			if(n % 3 !== 0 || n === totalArr.length) {
				totalArrWithComa.unshift(totalArr[totalArr.length - n]);
			} else {
				totalArrWithComa.unshift(',' + totalArr[totalArr.length - n]);
			}
		}
		totalArrWithComa.unshift('- ')
	}
	return (totalArrWithComa)
}