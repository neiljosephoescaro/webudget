import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import VerticalBar from './VerticalBar';

import { Container } from 'react-bootstrap'; //temporary style

export default function Dashboard() {
	const { token } = useContext(UserContext);

	const [ incomeRecords, setIncomeRecords] = useState([]);
	const [ expenseRecords, setExpenseRecords] = useState([]);
	const [ incomePerMonth, setIncomePerMonth ] = useState([]);
	const [ expensePerMonth, setExpensePerMonth ] = useState([]);
	const [ incomeData, setIncomeData] = useState({});
	const [ expenseData, setExpenseData] = useState({});

	useEffect(()=>{
		if(token !== null) {
			const payload = {
	      headers: {Authorization: `Bearer ${ token }`} 
	    }

			fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}api/users/get-records`, payload)
			.then(res => res.json())
			.then(data => {
				if(data.length > 0) {
					setIncomeRecords(data.filter(record => {
						return record.type === 'income';
					}))
					setExpenseRecords(data.filter(record => {
						return record.type === 'expense';
					}))
				}
			})
		}
	}, [token])

	useEffect(()=> {
		let incomeArr = [0,0,0,0,0,0,0,0,0,0,0,0]
		incomeRecords.forEach(record => {
			incomeArr[new Date(record.dateAdded).getMonth()] += record.amount;
		})
		setIncomePerMonth(incomeArr);
		let expenseArr = [0,0,0,0,0,0,0,0,0,0,0,0]
		expenseRecords.forEach(record => {
			expenseArr[new Date(record.dateAdded).getMonth()] += record.amount;
		})
		setExpensePerMonth(expenseArr);
	}, [incomeRecords, expenseRecords])

	useEffect(()=>{
		setIncomeData({
			label: 'Income Per Month',
			bgColor: 'rgba(43, 155, 68, .3)',
			borderColor: 'rgba(43, 155, 68, 1)',
			hoverBgColor: 'rgba(43, 155, 68, 1)',
			hoverBorderColor: 'rgba(43, 155, 68, 1)',
			dataArray: incomePerMonth
		})
		setExpenseData({
			label: 'Expense Per Month',
			bgColor: 'rgb(255,99,71,.3)',
			borderColor: 'rgb(255,99,71,1)',
			hoverBgColor: 'rgb(255,99,71,1)',
			hoverBorderColor: 'rgb(255,99,71,1)',
			dataArray: expensePerMonth
		})
	}, [incomePerMonth, expensePerMonth])

	return (
		<React.Fragment>
			<Container className='mb-3 bg-light'>
				<VerticalBar inputData={incomeData}/>
			</Container>
			<Container className='mt-3 bg-light'>
				<VerticalBar inputData={expenseData}/>
			</Container>
		</React.Fragment>
	)
}