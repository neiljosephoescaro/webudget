import Router from 'next/router';
import Link from 'next/link'
import { useState, useContext } from 'react';
import UserContext from '../UserContext';
import { GoogleLogin } from 'react-google-login';
import styles from '../styles/LogIn.module.css';
import Swal from 'sweetalert2';

export default function Home() {

  const {setToken} = useContext(UserContext)

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [ emailInputStyle, setEmailInputStyle ] = useState({});
  const [ emailLabelStyle, setEmailLabelStyle ] = useState({});
  const [ passwordInputStyle, setPasswordInputStyle ] = useState({});
  const [ passwordLabelStyle, setPasswordLabelStyle ] = useState({});

  const authenticate = (e) => {
    e.preventDefault()
    const options = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json'  },
        body: JSON.stringify({ 
          email: email, 
          password: password,
          timeLogIn: new Date().toString()
        })
    }
    
    fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}api/users/login`, options)
    .then(res => res.json())
    .then(data => {
        if (typeof data.accessToken !== 'undefined') {
            localStorage.setItem('token', data.accessToken)
            setToken(data.accessToken)
            Router.push('/');
        } else {
            if (data.error === 'does-not-exist') {
              Swal.fire(
                'Error!',
                'Your email is not yet registered.',
                'error'
              )
            } else if (data.error === 'incorrect-password') {
              Swal.fire(
                'Error!',
                'Password Incorrect.',
                'error'
              )
            } else if (data.error === 'login-type-error') {
              Swal.fire(
                'Error!',
                'Login Type Error.',
                'error'
              )
            }
        }
    })
  }

  const authenticateGoogleToken = (response) => {
    console.log(response)

    const payload = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          tokenId: response.tokenId,
          timeLogIn: new Date().toString()
        })
    }

    fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}api/users/verify-google-token-id`, payload)
    .then(res => res.json())
    .then(data => {
      console.log(data)
      if (typeof data.accessToken !== 'undefined') {
        localStorage.setItem('token', data.accessToken)
        setToken(data.accessToken)
        Router.push('/');
      } else {
        if (data.error === 'google-auth-error') {
          Swal.fire(
            'Error!',
            'Google server error.',
            'error'
          )
        } else if (data.error === 'login-type-error') {
          Swal.fire(
            'Error!',
            'Log-in type error.',
            'error'
          )
        }
      }
    })
  }

  function changeEmailStyle(e) {
    setEmail(e.target.value)
    if(e.target.value !== '') {
      setEmailLabelStyle ({
        top: '-25%',
        fontSize: '.8rem'
      })
      setEmailInputStyle({
        zIndex: '0'
      })
    } else {
      setEmailLabelStyle({})
      setEmailInputStyle({})
    }
  }

  function changePasswordStyle(e) {
    setPassword(e.target.value)
    if(e.target.value !== '') {
      setPasswordLabelStyle ({
        top: '-25%',
        fontSize: '.8rem'
      })
      setPasswordInputStyle({
        zIndex: '0'
      })
    } else {
      setPasswordLabelStyle({})
      setPasswordInputStyle({})
    }
  }

  return (
    <main className={styles.main}>
      <h1 className={styles.headerOne}>Welcome to WE<span className={styles.budget}>Budget</span> App</h1>
      <section className={styles.logInSection}>
        <h2 className={styles.headerTwo}>BUDGET NOW!</h2>
        <hr className={styles.horizontalLine}/>
        <form className={styles.form} onSubmit={ authenticate }>
          <div className={styles.inputSection}>
            <input className={styles.input} style={emailInputStyle} type='email' value={ email } onChange={ (e) => changeEmailStyle(e) } autoComplete="off" required/>
            <div className={styles.inputLabel} style={emailLabelStyle}> Email Address</div>
          </div>
          <div className={styles.inputSection}>
            <input className={styles.input} style={passwordInputStyle} type='password' value={ password } onChange={ (e) => changePasswordStyle(e) } autoComplete="off" required/>
            <div className={styles.inputLabel} style={passwordLabelStyle}> Password</div>
          </div>
          <button className={styles.logInButton}>LOG IN</button>
          <Link href="/register">
              <button className={styles.signUpButton}>SIGN UP</button>
          </Link>
          <GoogleLogin
              clientId="452803331692-2k8o9hbho24f3roktk4p36od42fu7o0h.apps.googleusercontent.com"
              buttonText="LOGIN"
              onSuccess={ authenticateGoogleToken }
              onFailure={ authenticateGoogleToken }
              cookiePolicy={ 'single_host_origin' }
              className={styles.googleButton}
          />
        </form>
      </section>
    </main>
  )
}
