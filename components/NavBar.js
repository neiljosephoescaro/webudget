import Link from 'next/link'
import { useContext } from 'react'
import UserContext from '../UserContext'

import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'

export default function NavBar() {
    const { token } = useContext(UserContext)

    return (
        <Navbar bg="dark" fixed='top' expand="lg">
            <Link href="/">
                <a className="webudget"><img className='web' src='wbLogoGray.png'/><span className='udget'>UDGET</span></a>
            </Link>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="navitems">
                    {
                        ( token !== null ) ?
                        <>  
                            <Link href="/records">
                                <a className="nav-link" role="button"><span className="navLink">RECORDS</span></a>
                            </Link>
                            <Link href="/categories">
                                <a className="nav-link" role="button"><span className="navLink">CATEGORIES</span></a>
                            </Link>
                            <Link href="/logout">
                                <a className="nav-link" role="button"><span className="navLinkOut">LOG OUT</span></a>
                            </Link>
                        </>
                        : 
                        <>
                        </>
                    }
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}
