import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import Dashboard from './Dashboard';
import styles from '../styles/Profile.module.css';

export default function Profile() {
	const { token } = useContext(UserContext);

	const [ lastVisit, setLastVisit ] = useState('')
	const [ firstInitial, setFirstInitial ] = useState('')
	const [ lastInitial, setLastInitial ] = useState('')
	const [ name, setName ] = useState('')
	const [ balance, setBalance] = useState(0);
	const [ isPositive, setIsPositive ] = useState(true);

	useEffect(() => {
		const payload = {
	        headers: { Authorization: `Bearer ${ token }`}
	    }

	    fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}api/users/details`, payload)
	    .then(res => res.json())
	    .then(data => {
	    	let fNameArr = [...data.firstName];
	    	let lNameArr = [...data.lastName]; 
	    	let fi = fNameArr.shift();
	    	let li = lNameArr.shift();
	    	setFirstInitial(fi);
	    	setLastInitial(li);
	    	if(data.timeLogOut.length > 0) {
	    		let lTimeOut = data.timeLogOut.pop();
	    		setLastVisit(new Date(lTimeOut).toLocaleString());
	    	} else {
	    		setLastVisit('Just Now')
	    	}
	    	setName(() => {
	    		return <span className={styles.userName}>{data.firstName}&nbsp;{data.lastName}</span>
	    	})
	    	if(data.transactions.length > 0) {
	    		let total = 0;
    		    data.transactions.forEach( record =>{
    		        if(record.type === 'income'){
    		            total += parseInt(record.amount);
    		        } else {
    		            total -= parseInt(record.amount);
    		        }
    		    })
    		    if(total < 0) {
    		    	setIsPositive(false)
    		    } else {
    		    	setIsPositive(true)
    		    }
	    		let totalStr = String(total);
	 			let totalArr = [...totalStr]
	 			if(totalArr[0] !== '-') {
	 				let n, totalArrWithComa = [];
		    		for(n = 1 ; n <= totalArr.length ; n++) {
		    			if(n % 3 !== 0 || n === totalArr.length) {
		    				totalArrWithComa.unshift(totalArr[totalArr.length - n]);
		    			} else {
		    				totalArrWithComa.unshift(',' + totalArr[totalArr.length - n]);
		    			}
		    		}
		    		setBalance(totalArrWithComa)
		    	} else {
		    		let n, totalArrWithComa = [];
		    		totalArr.shift(totalArr[0]);
		    		for(n = 1 ; n <= totalArr.length ; n++) {
		    			if(n % 3 !== 0 || n === totalArr.length) {
		    				totalArrWithComa.unshift(totalArr[totalArr.length - n]);
		    			} else {
		    				totalArrWithComa.unshift(',' + totalArr[totalArr.length - n]);
		    			}
		    		}
		    		totalArrWithComa.unshift('- ')
		    		setBalance(totalArrWithComa)
		    	}
	    	}
	    })
	}, [token])

	return (
		<main className={styles.main}>
			<div className={styles.walletSection}>
				<h1 className={styles.headerOne}>
					<span className={styles.myWallet}>MY WALLET</span>
					<span className={styles.lastVisit}>Last Visit: {lastVisit}</span>
				</h1>
				<hr className={styles.horizontalLine}/>
				<h2 className={styles.headerTwo}>
					<span>
						<span className={styles.avatarLogo}>{firstInitial}{lastInitial}</span>
						{name}
					</span>
					<span>
						<span className={styles.balance}>BALANCE:&nbsp;</span>
						<span className={styles.pesoSign}>&#8369;</span>&nbsp;
						{isPositive ?
							<span className={styles.posBalance}>{balance}</span>
							:
							<span className={styles.negBalance}>{balance}</span>
						}
					</span>
				</h2>
				<hr className={styles.horizontalLine}/>
				<h3 className={styles.headerThree}>Dashboard</h3>
				<Dashboard/>
			</div>
		</main>
	)
}