import { useState, useEffect } from 'react'
import { Bar } from 'react-chartjs-2'

export default function VerticalBar({inputData}) {
	const data = {
	      labels: ['January','February','March','April','May','June','July','August','September','October','November','December'],
	      datasets: [
	          {
	              label: inputData.label,
	              backgroundColor: inputData.bgColor,
	              borderColor: inputData.borderColor,
	              borderWidth: 1,
	              hoverBackgroundColor: inputData.hoverBgColor,
	              hoverBorderColor: inputData.hoverBorderColor,
	              data: inputData.dataArray
	          }
	      ]
	  }

	  return (
	  	<Bar data={data}/>
	  )
}