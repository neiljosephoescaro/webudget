import Head from 'next/head'
import '../styles/globals.css'
import { useState, useEffect } from 'react';
import { UserProvider } from '../UserContext';
import NavBar from '../components/NavBar';

import 'bootstrap/dist/css/bootstrap.min.css';


function MyApp({ Component, pageProps }) {
	const [token , setToken] = useState(null);

	useEffect(() => {
		setToken(localStorage.getItem('token'));
	}, [])

	const clearToken = () => {
    localStorage.clear();
    setToken(null);
  }

  return (
  	<React.Fragment>
  		<Head>
  			<meta name="viewport" content="width=device-width, initial-scale=1,shrink-to-fit=no"/>
        <meta name="keywords" content="Budget, Budget App, Web project, Budget Tracker, next.js project, webudget"/>
        <meta name="description" content="Budget Tracker Web App"/>
        <meta name="author" content="Neil Joseph Escaro"/>
        <link rel="icon" href="wbfavicon.png" sizes="32x32" type="image/png"/>
  			<link href="https://fonts.googleapis.com/css2?family=NTR&family=Orbitron&family=Plaster&family=Public+Sans:wght@100&display=swap" rel="stylesheet"/>
			</Head>	
			<UserProvider value={{token, setToken, clearToken}}>
	  		<NavBar/>
	  		<Component {...pageProps} />
  		</UserProvider>
  	</React.Fragment>
  )
}

export default MyApp
