import Head from 'next/head';
import Router from 'next/router';
import Link from 'next/link';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../../UserContext';
import styles from '../../styles/AddCategory.module.css';
import Swal from 'sweetalert2';

export default function addCategory() {
	const { token } = useContext(UserContext);

	const [ categoryName, setCategoryName ] = useState('');
	const [ categoryType, setCategoryType ] = useState('income');
	const [ readyToAdd, setReadyToAdd ] = useState(false);

	const [ categoryNameInputStyle, setCategoryNameInputStyle ] = useState({});
  	const [ categoryNameLabelStyle, setCategoryNameLabelStyle ] = useState({});

	useEffect(() => {
		if(categoryName !== '') {
			setReadyToAdd(true);
		} else {
			setReadyToAdd(false);
		}
	}, [categoryName])

	function addCategory(e){
		e.preventDefault()
		const payload = {
	        method: 'POST',
	        headers: { 
		      	'Content-Type': 'application/json',
		      	Authorization: `Bearer ${ token }` 
	    	},
	        body: JSON.stringify({ 
		      	name: categoryName,
		      	typeName: categoryType
	        })
	    }

	    fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}api/users/add-category`, payload)
	    .then(res => res.json())
	    .then(data => {
	    	if(data === true) {
	    		Swal.fire(
	    		  'Success!',
	    		  'Category Added.',
	    		  'success'
	    		)
	    		setCategoryName('')
	    	} else {
	    		Swal.fire(
	    		  'Error!',
	    		  'Something went wrong.',
	    		  'error'
	    		)
	    	}
	    })
	}

	function changeEmailStyle(e) {
    setCategoryName(e.target.value)
    if(e.target.value !== '') {
      setCategoryNameLabelStyle ({
        top: '-25%',
        fontSize: '.8rem'
      })
      setCategoryNameInputStyle({
        zIndex: '0'
      })
    } else {
      setCategoryNameLabelStyle({})
      setCategoryNameInputStyle({})
    }
  }

	return (
		<main className={styles.main}>
			<form className={styles.addCategorySection} onSubmit={e => addCategory(e)}>
				<h1 className={styles.addCategory}>ADD CATEGORY</h1>
				<hr className={styles.horizontalLine}/>
				<div className={styles.inputSection}>
				  <input className={styles.input} style={categoryNameInputStyle} type='text' value={categoryName} onChange={ (e) => changeEmailStyle(e)} autoComplete="off" required/>
				  <div className={styles.inputLabel} style={categoryNameLabelStyle}>Category Name</div>
				</div>
				<div className={styles.typeButtons}>
					<span>
				    	<label className={styles.typeLabel}>SELECT TYPE:</label>
					    <select className={styles.typeSelect} value={categoryType} onChange={e => setCategoryType(e.target.value)}>
						    <option value="income">INCOME</option>
						    <option value="expense">EXPENSE</option>
						</select>
					</span>
					<span>
					{
					  	(readyToAdd) ?
					  	<button className={styles.saveButton} type='submit'>SAVE</button>
					  	:
					  	<button className={styles.saveButtonDisabled} type='submit' disabled>SAVE</button>
					} 
					<Link href="/categories">
			            <button className={styles.cancelButton}>LIST</button>
			        </Link>
					</span>
				</div>
			</form>
		</main>
	)
}