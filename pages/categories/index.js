import Head from 'next/head';
import Link from 'next/link'
import Router from 'next/router';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../../UserContext';
import styles from '../../styles/Categories.module.css'
import Swal from 'sweetalert2';


export default function index() {

	const { token } = useContext(UserContext);

	const [ categoryList, setCategoryList ] = useState('');
	const [ categoryType, setCategoryType ] = useState('all');

	const payload = {
  		headers: {Authorization: `Bearer ${ token }`} 
	}

    fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}api/users/get-categories`, payload)
    .then(res => res.json())
    .then(data => {
    	if(data.length > 0){
    		let catList;
    		switch(categoryType) {
    			case "income":
    				catList = data.filter(record => {
    					return record.type === 'income';
    				})
    				break;
    			case "expense":
    				catList = data.filter(record => {
    					return record.type === 'expense';
    				})
    				break;
    			default:
    				catList = data;
    		}
    		let colorStyle;
	    	setCategoryList(catList.map(category => {
	    		if(category.type === 'income') {
	    			colorStyle = {color: '#ADFF2F'}
	    		} else {
	    			colorStyle = {color: 'rgb(255,99,71)'}
	    		}
	    		return (
	    			<div className={styles.categoryRow} key={category._id}>
	    				<span className={styles.nameType}>
			    			<span className={styles.categoryName}>{category.name}</span>
			    			<span className={styles.categoryType} style={colorStyle}>{category.type.toUpperCase()}</span>
			    		</span>
		    			<button className={styles.deleteButton} onClick={() => deleteCategory(category._id)}>DELETE</button>
	    			</div>
	    		)
	    	}))
    	} else {
    		setCategoryList('No Category Available')
    	}
    })

	useEffect(() => {
		const payload = {
      		headers: {Authorization: `Bearer ${ token }`} 
    	}

	    fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}api/users/get-categories`, payload)
	    .then(res => res.json())
	    .then(data => {
	    	if(data.length > 0){
	    		let catList;
	    		switch(categoryType) {
	    			case "income":
	    				catList = data.filter(record => {
	    					return record.type === 'income';
	    				})
	    				break;
	    			case "expense":
	    				catList = data.filter(record => {
	    					return record.type === 'expense';
	    				})
	    				break;
	    			default:
	    				catList = data;
	    		}
	    		let colorStyle;
		    	setCategoryList(catList.map(category => {
		    		if(category.type === 'income') {
		    			colorStyle = {color: '#ADFF2F'}
		    		} else {
		    			colorStyle = {color: 'rgb(255,99,71)'}
		    		}
		    		return (
		    			<div className={styles.categoryRow} key={category._id}>
		    				<span className={styles.nameType}>
				    			<span className={styles.categoryName}>{category.name}</span>
				    			<span className={styles.categoryType} style={colorStyle}>{category.type.toUpperCase()}</span>
				    		</span>	
				    		<button className={styles.deleteButton} onClick={() => deleteCategory(category._id)}>DELETE</button>
		    			</div>
		    		)
		    	}))
	    	} else {
	    		setCategoryList('No Category Available')
	    	}
	    })
	},[ categoryType ])

	function deleteCategory(categoryId) {
		Swal.fire({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: 'green',
		  cancelButtonColor: 'rgb(212,175,55)',
		  confirmButtonText: 'Yes, delete it!'
		}).then((result) => {
		    if (result.isConfirmed) {
	  			const payload = {
	  	      		method: 'DELETE',
	  	      		headers: { 
	  	      			'Content-Type': 'application/json',
	  	      			Authorization: `Bearer ${ token }` 
	  	    		},
	  	      		body: JSON.stringify({ 
	  	      			categoryId: categoryId
	  	      		})
	  	    	}

	  		    fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}api/users/delete-category`, payload)
	  		    .then(res => res.json())
	  		    .then(data => {
	  		    	if(data === true) {
	  		    		Swal.fire(
	  		    		  'Deleted!',
	  		    		  'Category has been deleted.',
	  		    		  'success'
	  		    		)
	  		    		setCategoryType('income');
	  		    		setCategoryType('all');
	  		    	} else {
	  		    		Swal.fire(
	  		    		  'Error!',
	  		    		  'Something went wrong.',
	  		    		  'error'
	  		    		)
	  		    	}
	  		    })
		    }
		})
	}

	return (
		<React.Fragment>
			<Head>
				<title>Categories</title>
			</Head>
			<main className={styles.main}>
				<section className={styles.categoryListSection}>
					<div className={styles.categoryListHeader}>
						<span className={styles.categoryList}>CATEGORY LIST</span>
						<select className={styles.selectCategoryType} value={categoryType} onChange={e => setCategoryType(e.target.value)}>
							<option className={styles.optionCategoryType} value="all">ALL</option>
						    <option className={styles.optionCategoryType} value="income">INCOME</option>
						    <option className={styles.optionCategoryType} value="expense">EXPENSE</option>
						</select>
						<Link href={'/categories/addCategory'}>
							<button className={styles.addButton}>ADD</button>
						</Link>
					</div>
					<hr className={styles.horizontalLine}/>
					{categoryList}
				</section>
			</main>
		</React.Fragment>
	)
}

