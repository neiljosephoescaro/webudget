import Head from 'next/head';
import Router from 'next/router';
import Link from 'next/link';
import LogIn from '../components/LogIn';
import Profile from '../components/Profile';
import { useState, useContext } from 'react';
import UserContext from '../UserContext';

import { Form, Button, Card, Row, Col, Container } from 'react-bootstrap';

export default function Home() {

  const { token } = useContext(UserContext)

  return (
    <React.Fragment>
      <Head>
        <title>WEBudget</title>
      </Head>
      {
        ( token === null ) ?
        <LogIn/>
        :
        <Profile/>
      }
    </React.Fragment>
  )
}
