import { useContext } from 'react';
import UserContext from '../UserContext';
import Router from 'next/router';
import Swal from 'sweetalert2';

export default function logout(){
	const { token , clearToken} = useContext(UserContext)

	const payload = {
    method: 'PUT',
    headers: { 
      'Content-Type': 'application/json',
      Authorization: `Bearer ${ token }` 
    },
    body: JSON.stringify({
      timeLogOut: new Date().toString()
    })
  }

  fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}api/users/logout`, payload)
  .then(res => res.json())
  .then(data => {
  	if(data === true){
	    clearToken();
      Swal.fire(
        'Successfully Log Out!',
        'See you again.',
        'success'
      )
			Router.push('/')
		}
  })

  return null
}