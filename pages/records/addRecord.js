import Head from 'next/head';
import Link from 'next/link';
import Router from 'next/router';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../../UserContext';
import styles from '../../styles/AddAndEditRecord.module.css';
import Swal from 'sweetalert2';

export default function addRecord() {
	const { token } = useContext(UserContext);

	const [ categoryType, setCategoryType ] = useState('income');
	const [ categoryName, setCategoryName ] = useState('');
	const [ categoryNameList, setCategoryNameList ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ amount, setAmount ] = useState('');
	const [ readyToAdd, setReadyToAdd ] = useState(false);

	const [ descriptionInputStyle, setDescriptionInputStyle ] = useState({});
    const [ descriptionLabelStyle, setDescriptionLabelStyle ] = useState({});
    const [ amountInputStyle, setAmountInputStyle ] = useState({});
    const [ amountLabelStyle, setAmountLabelStyle ] = useState({});

	useEffect(() => {
		const payload = {
      headers: {Authorization: `Bearer ${ token }`} 
    }

    fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}api/users/get-categories`, payload)
    .then(res => res.json())
    .then(data => {
    	let catArr;
    	if(data.length > 0) {
		    if(categoryType === 'income') {
		    	catArr = data.filter(category => {
		    		return category.type === 'income';
		    	})
		    	setCategoryNameList(catArr.map(cat => {
		    		return <option key={cat._id} value={cat.name}>{cat.name}</option>
		    	}))
		    } else {
		    	catArr = data.filter(category => {
		    		return category.type === 'expense';
		    	})
		    	setCategoryNameList(catArr.map(cat => {
		    		return <option key={cat._id} value={cat.name}>{cat.name}</option>
		    	}))
		    }
		    setCategoryName(catArr[0].name)
		}
    })
	}, [categoryType, token])

	useEffect(() => {
		if(amount !== '' && amount !== 0) {
			setReadyToAdd(true);
		} else {
			setReadyToAdd(false);
		}
	}, [amount])

	function addRecord(e){
		e.preventDefault()
		const payload = {
		      method: 'POST',
		      headers: { 
		      	'Content-Type': 'application/json',
		      	Authorization: `Bearer ${ token }` 
		    	},
		      body: JSON.stringify({ 
		      	categoryName: categoryName,
		      	typeName: categoryType,
		      	description: description,
		      	amount: amount,
		      	dateAdded: new Date().toString()
		      })
	    }

	    fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}api/users/add-record`, payload)
	    .then(res => res.json())
	    .then(data => {
	    	if(data === true) {
	    		Swal.fire(
	    		  'Success!',
	    		  'Record Added.',
	    		  'success'
	    		)
	    		setDescription('')
	    		setAmount(0)
	    	} else {
	    		Swal.fire(
	    		  'Error!',
	    		  'Something went wrong.',
	    		  'error'
	    		)
	    	}
	    })
	}

	function changeDescriptionStyle(e) {
	    setDescription(e.target.value)
	    if(e.target.value !== '') {
	        setDescriptionLabelStyle ({
	        	top: '-25%',
	        	fontSize: '.8rem'
	        })
	        setDescriptionInputStyle({
	        	zIndex: '0'
	        })
	    } else {
	        setDescriptionLabelStyle({})
	        setDescriptionInputStyle({})
	    }
	}

	function changeAmountStyle(e) {
	    setAmount(e.target.value)
	    if(e.target.value !== '') {
	        setAmountLabelStyle ({
	        	top: '-25%',
	        	fontSize: '.8rem'
	        })
	        setAmountInputStyle({
	        	zIndex: '0'
	        })
	    } else {
	        setAmountLabelStyle({})
	        setAmountInputStyle({})
	    }
	}

	return (
		<main className={styles.main}>
			<form className={styles.addRecordSection} onSubmit={e => addRecord(e)}>
				<h1 className={styles.addRecord}>ADD RECORD</h1>
				<div className={styles.selectTypeSection}>
				    <label className={styles.typeLabel}>SELECT TYPE:</label>
				    <select className={styles.typeSelect} value={categoryType} onChange={e => setCategoryType(e.target.value)}>
					    <option value="income">INCOME</option>
					    <option value="expense">EXPENSE</option>
					</select>
				</div>
				<hr className={styles.horizontalLine}/>
				<label className={styles.nameLabel}>CHOOSE NAME:</label><br/>
			    <select className={styles.nameSelect} value={categoryName} onChange={e => setCategoryName(e.target.value)}>
			    	{categoryNameList}
			    </select><br/>
			    <div className={styles.inputSection}>
		            <input className={styles.input} style={descriptionInputStyle} type='text' value={description} onChange={ (e) => changeDescriptionStyle(e) } autoComplete="off" required/>
		            <div className={styles.inputLabel} style={descriptionLabelStyle}>DESCRIPTION</div>
		        </div>
		        <div className={styles.inputSection}>
		            <input className={styles.input} style={amountInputStyle} type='number' value={amount} onChange={ (e) => changeAmountStyle(e) } autoComplete="off" required/>
		            <div className={styles.inputLabel} style={amountLabelStyle}>AMOUNT</div>
		        </div>
			    <div>
					{
					  	(readyToAdd) ?
					  	<button className={styles.saveButton} type='submit'>SAVE</button>
					  	:
					  	<button className={styles.saveButtonDisabled} type='submit' disabled>SAVE</button>
					} 
					<Link href="/records">
			            <button className={styles.cancelButton}>RECORD LIST</button>
			        </Link>
				</div>
			</form>
		</main>
	)
}