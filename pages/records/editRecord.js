import Router from 'next/router';
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useState, useEffect, useContext } from 'react';
import UserContext from '../../UserContext';
import styles from '../../styles/AddAndEditRecord.module.css';
import Swal from 'sweetalert2';


export default function editRecord() {
	const { token } = useContext(UserContext);

	const router = useRouter();
    const recordId = router.query.recordId;

    const [ categoryNameList, setCategoryNameList ] = useState('');
    const [ categoryType, setCategoryType] = useState('');
    const [ categoryName, setCategoryName] = useState('');
    const [ description, setDescription] = useState('');
    const [ amount, setAmount] = useState(0);
    const [ newCategoryType, setNewCategoryType] = useState('');
    const [ newCategoryName, setNewCategoryName] = useState('');
    const [ newDescription, setNewDescription] = useState('');
    const [ newAmount, setNewAmount] = useState(0);
    const [ dateAdded, setDateAdded ] = useState('');

    const [ descriptionInputStyle, setDescriptionInputStyle ] = useState({});
    const [ descriptionLabelStyle, setDescriptionLabelStyle ] = useState({});
    const [ amountInputStyle, setAmountInputStyle ] = useState({});
    const [ amountLabelStyle, setAmountLabelStyle ] = useState({});

    useEffect(()=> {
		const payload = {
      headers: {Authorization: `Bearer ${ token }`} 
    }

    fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}api/users/get-records`, payload)
    .then(res => res.json())
    .then(data => {
    	if(data.length > 0){
    		const recordToEdit = data.find(record => {
    			return record._id === recordId;
    		})

    		console.log(recordToEdit.dateAdded)
    		setCategoryType(recordToEdit.type)
    		setNewCategoryType(recordToEdit.type)
    		setCategoryName(recordToEdit.categoryName)
    		setNewCategoryName(recordToEdit.categoryName)
    		setDescription(recordToEdit.description)
    		setNewDescription(recordToEdit.description)
    		setAmount(recordToEdit.amount)
    		setNewAmount(recordToEdit.amount)
    		setDateAdded(new Date(recordToEdit.dateAdded).toLocaleString())
    	}
    })
  }, [recordId])

  useEffect(() => {

		const payload = {
      headers: {Authorization: `Bearer ${ token }`} 
    }

    fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}api/users/get-categories`, payload)
    .then(res => res.json())
    .then(data => {
    	let catArr;
    	if(data.length > 0) {
		    if(newCategoryType === 'income') {
		    	catArr = data.filter(category => {
		    		return category.type === 'income';
		    	})
		    	setCategoryNameList(catArr.map(cat => {
		    		return <option key={cat._id} value={cat.name}>{cat.name}</option>
		    	}))
		    } else {
		    	catArr = data.filter(category => {
		    		return category.type === 'expense';
		    	})
		    	setCategoryNameList(catArr.map(cat => {
		    		return <option key={cat._id} value={cat.name}>{cat.name}</option>
		    	}))
		    }
		  }
    })
  }, [newCategoryType, token])

	function editRecord(e) {
		e.preventDefault()

		const payload = {
	    method: 'PUT',
	    headers: { 
	    	'Content-Type': 'application/json',
	    	Authorization: `Bearer ${ token }` 
	  	},
	    body: JSON.stringify({ 
	    	recordId: recordId,
	    	newCategoryName: newCategoryName,
	    	newType: newCategoryType,
	    	newDescription: newDescription,
	    	newAmount: newAmount
	    })
	  }

	  fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}api/users/edit-record`, payload)
	  .then(res => res.json())
	  .then(data => {
	  	if(data === true) {
    		Swal.fire(
    		  'Success!',
    		  'Record Edited.',
    		  'success'
    		)
    		Router.push('/records')
    	} else {
    		Swal.fire(
    		  'Error!',
    		  'Something went wrong.',
    		  'error'
    		)
    	}
	  })
	}

	function changeDescriptionStyle(e) {
	    setNewDescription(e.target.value)
	    if(e.target.value !== '') {
	        setDescriptionLabelStyle ({
	        	top: '-25%',
	        	fontSize: '.8rem'
	        })
	        setDescriptionInputStyle({
	        	zIndex: '0'
	        })
	    } else {
	        setDescriptionLabelStyle({})
	        setDescriptionInputStyle({})
	    }
	}

	function changeAmountStyle(e) {
	    setNewAmount(e.target.value)
	    if(e.target.value !== '') {
	        setAmountLabelStyle ({
	        	top: '-25%',
	        	fontSize: '.8rem'
	        })
	        setAmountInputStyle({
	        	zIndex: '0'
	        })
	    } else {
	        setAmountLabelStyle({})
	        setAmountInputStyle({})
	    }
	}

	return (
		<main className={styles.main}>
			<form className={styles.addRecordSection} onSubmit={e => editRecord(e)}>
				<h1 className={styles.addRecord}>EDIT {new Date(dateAdded).toLocaleString()} RECORD</h1>
				<div className={styles.selectTypeSection}>
				    <label className={styles.typeLabel}>TYPE: <span className={styles.oldType}>{categoryType.toUpperCase()}</span> </label>
				    <select className={styles.typeSelect} value={newCategoryType} onChange={e => setNewCategoryType(e.target.value)}>
					    <option value="income">INCOME</option>
					    <option value="expense">EXPENSE</option>
					</select>
				</div>
				<hr className={styles.horizontalLine}/>
				<label className={styles.nameLabel}>NAME: <span className={styles.oldName}>{categoryName}</span> </label><br/>
			    <select className={styles.nameSelect} value={newCategoryName} onChange={e => setNewCategoryName(e.target.value)}>
			    	{categoryNameList}
			    </select><br/>
			    <div className={styles.inputSection}>
		            <input className={styles.input} style={descriptionInputStyle} type='text' onChange={e => changeDescriptionStyle(e)} autoComplete="off"/>
		            <div className={styles.inputLabel} style={descriptionLabelStyle}>{description}</div>
		        </div>
		        <div className={styles.inputSection}>
		            <input className={styles.input} style={amountInputStyle} type='number' onChange={e => changeAmountStyle(e)} autoComplete="off"/>
		            <div className={styles.inputLabel} style={amountLabelStyle}>{amount}</div>
		        </div>
			    <div>
					<button className={styles.saveButton} type='submit'>SAVE</button>
					<Link href="/records">
			            <button className={styles.cancelButton}>CANCEL</button>
			        </Link>
				</div>
			</form>
		</main>
	)
}

