import Head from 'next/head';
import Link from 'next/link';
import Router from 'next/router';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../../UserContext';
import ToString from "../../ToString";
import styles from '../../styles/Records.module.css'
import Swal from 'sweetalert2';


export default function index() {
	const { token } = useContext(UserContext);

	const [ searchTypeRecord , setSearchTypeRecord ] = useState('all');
	const [ sort, setSort] = useState('newest');
	const [ searchRecord, setSearchRecord ] = useState('');
	const [ recordList, setRecordList ] = useState([]);
	const [ finalRecordList, setFinalRecordList] = useState([]);
	const [ listGroupItem, setListGroupItem ] = useState('');

	useEffect(()=>{
		const payload = {
	      headers: {Authorization: `Bearer ${ token }`} 
	    }

	    fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}api/users/get-records`, payload)
	    .then(res => res.json())
	    .then(data => {
	    	if(data.length > 0){
	    		if(sort === 'newest') {//for sort
	    			data.sort((a,b) => {
		    			return (Number(new Date(b.dateAdded)) - Number(new Date(a.dateAdded)));
		    		})
	    		} else {
	    			data.sort((a,b) => {
		    			return (Number(new Date(a.dateAdded)) - Number(new Date(b.dateAdded)));
		    		})
	    		}
	    		//for filter
	    		switch(searchTypeRecord) {
	    			case "income":
	    				setRecordList(data.filter(record => {
	    					return record.type === 'income';
	    				}))
	    				break;
	    			case "expenses":
	    				setRecordList(data.filter(record => {
	    					return record.type === 'expense';
	    				}))
	    				break;
	    			default:
	    				setRecordList(data);
	    		}
	    	} else {
	    		setFinalRecordList('');
	    	}
	    })
	}, [searchTypeRecord, token , sort])
	//for search
	useEffect(() => {
		if(searchRecord !== '') {
			let patt = new RegExp(searchRecord.toLowerCase())
			setFinalRecordList(recordList.filter(record => {
				return (patt.test(record.categoryName.toLowerCase())
					|| patt.test(record.type)
					|| patt.test(record.description.toLowerCase()) 
					|| patt.test(record.amount.toString())
					|| patt.test(record.balanceAfterTransaction.toString())
					|| patt.test(record.dateAdded))
			}))
		} else {
			setFinalRecordList(recordList)
		}	
	},[recordList, searchRecord])
	//populating the final data for records
	useEffect(()=>{
		if(finalRecordList.length > 0) {
			let posStyle = {
				color: 'green'
			}
			let negStyle = {
				color: 'rgb(255,99,71)'
			}
			let amountStyle = {}, balanceStyle = {}, typeStyle = {};
			setListGroupItem(finalRecordList.map(record => {
				if (record.type === 'income') {
					amountStyle = posStyle;
					typeStyle = posStyle;
				}
				if (record.type === 'expense') {
					amountStyle = negStyle;
					typeStyle = negStyle;
				}
				if (record.balanceAfterTransaction > 0) {
					balanceStyle = posStyle;
				}
				if (record.balanceAfterTransaction < 0) {
					balanceStyle = negStyle;
				}
				return (
					<div className={styles.container} key={record._id}>
						<p className={styles.date}>{new Date(record.dateAdded).toLocaleString()}</p>
						<p>CATEGORY: {record.categoryName} <span style={typeStyle}>({record.type.toUpperCase()})</span></p>
						<p>DESCRIPTION: {record.description}</p>
						<p>AMOUNT: <span style={amountStyle}>{ToString(record.amount)}</span></p>
						<p>BALANCE: <strong style={balanceStyle}>{ToString(record.balanceAfterTransaction)}</strong></p>
						<p className={styles.buttons}>
							<Link href={{ pathname: '/records/editRecord', query: { recordId: record._id } }}>
								<button className={styles.editButton}>EDIT</button>
							</Link>
							<button className={styles.deleteButton} onClick={() => deleteRecord(record._id)}>DELETE</button>
						</p>
					</div>
				)
			}))
		} else {
			setListGroupItem(() => {
				return (
					<div>
						<p className='text-center'>No Record</p>
					</div>
				)
			})
		}
	},[finalRecordList])

	function deleteRecord(recordId) {
		Swal.fire({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: 'green',
		  cancelButtonColor: 'rgb(212,175,55)',
		  confirmButtonText: 'Yes, delete it!'
		}).then((result) => {
		    if (result.isConfirmed) {
	  			const payload = {
			        method: 'DELETE',
			        headers: { 
			      		'Content-Type': 'application/json',
			      		Authorization: `Bearer ${ token }` 
			    	},
			        body: JSON.stringify({ 
			      		recordId: recordId
			        })
			    }

			    fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}api/users/delete-record`, payload)
			    .then(res => res.json())
			    .then(data => {
	  		    	if(data === true) {
	  		    		Swal.fire(
	  		    		  'Deleted!',
	  		    		  'Record has been deleted.',
	  		    		  'success'
	  		    		)
	  		    		setSearchTypeRecord('income');
	  		    		setSearchTypeRecord('all');
	  		    	} else {
	  		    		Swal.fire(
	  		    		  'Error!',
	  		    		  'Something went wrong.',
	  		    		  'error'
	  		    		)
	  		    	}
	  		    })
		    }
		})
	}

	return (
		<React.Fragment>
			<Head>
				<title>Records</title>
			</Head>
			<main className={styles.main}>
				<section className={styles.recordsSection}>
					<h1 className={styles.myRecords}>MY RECORDS</h1>
					<hr className={styles.horizontalLine}/>
					<div className={styles.toolBar}>
						<input className={styles.searchBar} type='search' placeholder='Search record here' value={searchRecord} onChange={e => setSearchRecord(e.target.value)}/>
						<div>
							<select className={styles.selectType} value={searchTypeRecord} onChange={e => setSearchTypeRecord(e.target.value)}>
								<option value='all'>ALL</option>
								<option value='income'>INCOME</option>
								<option value='expenses'>EXPENSES</option>
							</select>
							<select className={styles.sort} value={sort} onChange={e => setSort(e.target.value)}>
								<option value='newest'>NEWEST</option>
								<option value='oldest'>OLDEST</option>
							</select>
							<Link href={'/records/addRecord'}>
								<button className={styles.addButton}>ADD</button>
							</Link>
						</div>
					</div>
					<div className={styles.listSection}>
						{listGroupItem}
					</div>
				</section>
			</main>
		</React.Fragment>
	)

}
