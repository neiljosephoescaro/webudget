import Router from 'next/router';
import Link from 'next/link'
import {useState, useEffect} from 'react';
import styles from '../styles/Register.module.css';
import Swal from 'sweetalert2';

export default function register() {
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
  const [email, setEmail] = useState('')
  const [password1, setPassword1] = useState('')
  const [password2, setPassword2] = useState('')
  const [isActive, setIsActive] = useState(false)

  const [ firstNameInputStyle, setFirstNameInputStyle ] = useState({});
  const [ firstNameLabelStyle, setFirstNameLabelStyle ] = useState({});
  const [ lastNameInputStyle, setLastNameInputStyle ] = useState({});
  const [ lastNameLabelStyle, setLastNameLabelStyle ] = useState({});
  const [ emailInputStyle, setEmailInputStyle ] = useState({});
  const [ emailLabelStyle, setEmailLabelStyle ] = useState({});
  const [ passwordInputStyle, setPasswordInputStyle ] = useState({});
  const [ passwordLabelStyle, setPasswordLabelStyle ] = useState({});
  const [ verifyPasswordInputStyle, setVerifyPasswordInputStyle ] = useState({});
  const [ verifyPasswordLabelStyle, setVerifyPasswordLabelStyle ] = useState({});

  useEffect(() => {
    if(firstName !== '' && lastName !== '' 
  	&& email !== '' && password1 !== '' 
  	&& password2 !== '' && password1 === password2){
        setIsActive(true);
    }else{
        setIsActive(false)
    }
  }, [firstName, lastName, email, password1, password2])

  function registerUser(e){
    e.preventDefault();
    if(email.match(/@gmail.com\b/i)) {
    	Swal.fire(
        'Error!',
        'This is a google account. You can log in using Google button',
        'error'
      )
    } else {
      fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}api/users/email-exists`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          email: email
        })
      })
      .then(res => res.json())
      .then(data => {
        if(data === false){
          const payload = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 
              firstName: firstName,
              lastName: lastName,
              email: email,
              password: password1
            })
          }

          fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}api/users/`, payload)
          .then(res => res.json())
          .then(data => {
            if(data === true) {
              Swal.fire(
                'Success!',
                'You are now registered.',
                'success'
              )
              Router.push('/');
            } else {
              Swal.fire(
                'Error!',
                'Something went wrong.',
                'error'
              )
            }
          })
        } else {
          Swal.fire(
            'Error!',
            'Your email is already used.',
            'error'
          )
        }
      }) 
	  }
	}

  function changeFirstNameStyle(e) {
    setFirstName(e.target.value)
    if(e.target.value !== '') {
      setFirstNameLabelStyle ({
        top: '-25%',
        fontSize: '.8rem'
      })
      setFirstNameInputStyle({
        zIndex: '0'
      })
    } else {
      setFirstNameLabelStyle({})
      setFirstNameInputStyle({})
    }
  }

  function changeLastNameStyle(e) {
    setLastName(e.target.value)
    if(e.target.value !== '') {
      setLastNameLabelStyle ({
        top: '-25%',
        fontSize: '.8rem'
      })
      setLastNameInputStyle({
        zIndex: '0'
      })
    } else {
      setLastNameLabelStyle({})
      setLastNameInputStyle({})
    }
  }

  function changeEmailStyle(e) {
    setEmail(e.target.value)
    if(e.target.value !== '') {
      setEmailLabelStyle ({
        top: '-25%',
        fontSize: '.8rem'
      })
      setEmailInputStyle({
        zIndex: '0'
      })
    } else {
      setEmailLabelStyle({})
      setEmailInputStyle({})
    }
  }

  function changePasswordStyle(e) {
    setPassword1(e.target.value)
    if(e.target.value !== '') {
      setPasswordLabelStyle ({
        top: '-25%',
        fontSize: '.8rem'
      })
      setPasswordInputStyle({
        zIndex: '0'
      })
    } else {
      setPasswordLabelStyle({})
      setPasswordInputStyle({})
    }
  }

  function changeVerifyPasswordStyle(e) {
    setPassword2(e.target.value)
    if(e.target.value !== '') {
      setVerifyPasswordLabelStyle ({
        top: '-25%',
        fontSize: '.8rem'
      })
      setVerifyPasswordInputStyle({
        zIndex: '0'
      })
    } else {
      setVerifyPasswordLabelStyle({})
      setVerifyPasswordInputStyle({})
    }
  }

  return (

  	<main className={styles.main}>
    	<form className={styles.signUpSection} onSubmit={(e) => registerUser(e)}>
        <h1 className={styles.headerOne}>SIGN UP HERE</h1>
        <hr className={styles.horizontalLine}/>
        <div className={styles.inputSection}>
          <input className={styles.input} style={firstNameInputStyle} type='text' value={firstName} onChange={ (e) => changeFirstNameStyle(e) } autoComplete="off" required/>
          <div className={styles.inputLabel} style={firstNameLabelStyle}>First Name</div>
        </div>
    		<div className={styles.inputSection}>
          <input className={styles.input} style={lastNameInputStyle} type='text' value={lastName} onChange={ (e) => changeLastNameStyle(e) } autoComplete="off" required/>
          <div className={styles.inputLabel} style={lastNameLabelStyle}>Last Name</div>
        </div>
        <div className={styles.inputSection}>
          <input className={styles.input} style={emailInputStyle} type='email' value={ email } onChange={ (e) => changeEmailStyle(e) } autoComplete="off" required/>
          <div className={styles.inputLabel} style={emailLabelStyle}> Email Address</div>
        </div>
        <div className={styles.inputSection}>
          <input className={styles.input} style={passwordInputStyle} type='password' value={ password1 } onChange={ (e) => changePasswordStyle(e) } autoComplete="off" required/>
          <div className={styles.inputLabel} style={passwordLabelStyle}>Password</div>
        </div>
        <div className={styles.inputSection}>
          <input className={styles.input} style={verifyPasswordInputStyle} type='password' value={ password2 } onChange={ (e) => changeVerifyPasswordStyle(e) } autoComplete="off" required/>
          <div className={styles.inputLabel} style={verifyPasswordLabelStyle}>Verify Password</div>
        </div>
        {isActive ?
            <button className={styles.registerButton}>REGISTER</button>
            :
            <button className={styles.registerButtonDisabled} disabled>REGISTER</button>
        }
        <Link href="/">
            <button className={styles.loginPageButton}>BACK TO LOGIN PAGE</button>
        </Link>
      </form>
  	</main>
  )
}